<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/20 0020
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>jquery-ajax</title>
    <%--引入jquery--%>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/jQuery-1.12.4/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        //使用jQuery进行ajax请求处理
        function doAjax() {
            //
            $.ajax({
                 //请求方式 get/post
                type: "get",
                //请求的是服务器的action的URL路径
                url:"doAjaxDemo",
                //data: "name=John&location=Boston",
                //回调函数 中的参数是服务器响应的内容
                success: function (data) {
                    // 从ajax引擎对象中，responseText属性中获取服务器响应的内容
                    $("#showMe").html(data);
                    console.info("ajax响应的内容");
                    console.info(data);
                }
                }
            )
        }
    </script>
</head>
<body>
<input type="button" id="ajax1" value="jquery的ajax" onclick="doAjax()">
<br>
<label id="showMe"></label>
</body>
</html>
